# About

This assets adds some (one at the moment) attributes for
[Odin Inspector](https://assetstore.unity.com/packages/tools/utilities/odin-inspector-and-serializer-89041)
, as well as tools, that makes life of custom attribute developer easier.


## Attributes

### [SelfOnly]

Constrains the field, so it's valid to link only other `Component`s of same `GameObject`.
Can be used only on a `Component` inside a `Monobehavior`.

Also, adds button to quickly `GetComponent<>` value.

#### Usage

```C#
[SerializeField, SelfOnly]
private RectTransform _selfRect;
```

### [ParentsOnly]

Constrains the field, so it's valid to link only to components of parent's objects in hierarchy. Works on scene and in Prefab mode.

TODO
- [ ] Draw button to for an auto-fix

#### Usage

```C#
[SerializeField, ParentsOnly]
private RectTransform _parentRect;
```

### [ItemCount]

Constrains the count of elements of marked collection: only collections with exact count are allowed.

```C#
[SerializeField, ItemCount(2)]
private List<int> _myList = new();
```

### [MaxItemCount]

Constrains the count of elements of marked collection: only collections with same or less element count are allowed.
Value is inclusive.

Can be combined with `[MinItemCount]`

```C#
[SerializeField, MaxItemCount(2)]
private List<int> _myList = new();
```

### [MinItemCount]

Constrains the count of elements of marked collection: only collections with same or more element count are allowed.
Value is inclusive.

Can be combined with `[MaxItemCount]`

```C#
[SerializeField, MinItemCount(2)]
private List<int> _myList = new();
```


## Tools

### Disposable Layouts

#### Idea

Syntax sugar, that allows developer of Custom Attributes, as well as Editor Windows, replace this:
```C#
protected override void DrawPropertyLayout(GUIContent label)
{
    EditorGUILayout.BeginHorizontal();
                
    EditorGUILayout.BeginVertical();
    this.CallNextDrawer(label);  
    EditorGUILayout.EndVertical();
    
    if (GUILayout.Button(text, EditorStyles.miniButton, GUILayoutOptions.ExpandWidth(false).MinWidth(ButtonMinWidth)))
    {
        this.Property.RecordForUndo($"Click {ButtonText}");
        this._clickAction.DoAction();
    }
    
    EditorGUILayout.EndHorizontal();
}
```
... with this:
```C#
protected override void DrawPropertyLayout(GUIContent label)
{
    using (new EditorGUILayoutHorizontal())
    {
        using (new EditorGUILayoutVertical())
            this.CallNextDrawer(label);
                    
        if (GUILayout.Button(ButtonText, EditorStyles.miniButton, GUILayoutOptions.ExpandWidth(false).MinWidth(ButtonMinWidth)))
        {
            this.Property.RecordForUndo($"Click {ButtonText}");
            this._clickAction.DoAction();
        }
    }
}
```

#### Currenty avaliable disposable wrappers
- `EditorGUILayoutHorizontal` - for `EditorGUILayout.`[ `Begin` | `End` ]`Horizontal()`
- `EditorGUILayoutVertical` - for `EditorGUILayout.`[ `Begin` | `End` ]`Vertical()`

#### Adding your own disposable wrapper

It's easy. You just need to inherit `` and to code required `Begin()` and `End()` methods.

Here's the example of `EditorGUILayoutVertical`:
```C#
public class EditorGUILayoutVertical : BaseDisposableLayout
	{
		protected override void Begin()
			=> EditorGUILayout.BeginVertical();


		protected override void End()
			=> EditorGUILayout.EndVertical();
	}
```

You are welcome to create MRs with new wrappers!