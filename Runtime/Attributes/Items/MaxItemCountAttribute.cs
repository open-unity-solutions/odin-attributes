using System;
using System.Diagnostics.CodeAnalysis;


namespace Valhalla.OdinAttributes
{
	[AttributeUsage(AttributeTargets.Field)]
	[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
	public class MaxItemCountAttribute : ValidatedInputAttribute
	{
		public readonly int MaxCount;

		public readonly string MaxCountGetter;


		public MaxItemCountAttribute(int maxCount)
		{
			if (maxCount < 0)
			{
				SetInputError($"{nameof(maxCount)} is {maxCount}, must be greater than 0");
				return;
			}
			
			MaxCount = maxCount;
			MaxCountGetter = null;
		}
		
		
		public MaxItemCountAttribute(Type enumType)
		{
			if (!enumType.IsEnum)
			{
				SetInputError($"Type {enumType} is not a Enum");
				return;
			}
			
			MaxCount = Enum.GetNames(enumType).Length;
			MaxCountGetter = null;
		}
		
		
		public MaxItemCountAttribute(string intGetter)
		{
			if (string.IsNullOrEmpty(intGetter))
			{
				SetInputError($"Expression must return int, but null given");
				return;
			}
			
			MaxCount = -1;
			MaxCountGetter = intGetter;
		}
	}
}
