using System;
using System.Diagnostics.CodeAnalysis;


namespace Valhalla.OdinAttributes
{
	[AttributeUsage(AttributeTargets.Field)]
	[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
	public class ItemCountAttribute : ValidatedInputAttribute
	{
		public readonly int ValidCount;
		
		public readonly string ValidCountGetter;
		
		
		public ItemCountAttribute(int validCount)
		{
			if (validCount < 0)
			{
				SetInputError($"{nameof(validCount)} is {validCount}, must be greater than 0");
				return;
			}
			
			ValidCount = validCount;
			ValidCountGetter = null;
		}
		
		
		public ItemCountAttribute(Type enumType)
		{
			if (!enumType.IsEnum)
			{
				SetInputError($"Type {enumType} is not a Enum");
				return;
			}
			
			ValidCount = Enum.GetNames(enumType).Length;
			ValidCountGetter = null;
		}


		public ItemCountAttribute(string intGetter)
		{
			if (string.IsNullOrEmpty(intGetter))
			{
				SetInputError($"Expression must return int, but null given");
				return;
			}
			
			ValidCount = -1;
			ValidCountGetter = intGetter;
		}
	}
}
