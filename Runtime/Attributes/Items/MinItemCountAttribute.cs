using System;
using System.Diagnostics.CodeAnalysis;


namespace Valhalla.OdinAttributes
{
	[AttributeUsage(AttributeTargets.Field)]
	[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
	public class MinItemCountAttribute : ValidatedInputAttribute
	{
		public readonly int MinCount;

		public readonly string MinCountGetter;

		public MinItemCountAttribute(int minCount)
		{
			if (minCount < 0)
			{
				SetInputError($"{nameof(minCount)} is {minCount}, must be greater than 0");
				return;
			}
			
			MinCount = minCount;
			MinCountGetter = null;
		}
		
		
		public MinItemCountAttribute(Type enumType)
		{
			if (!enumType.IsEnum)
			{
				SetInputError($"Type {enumType} is not a Enum");
				return;
			}
			
			MinCount = Enum.GetNames(enumType).Length;
			MinCountGetter = null;
		}
		
		
		public MinItemCountAttribute(string intGetter)
		{
			if (string.IsNullOrEmpty(intGetter))
			{
				SetInputError($"Expression must return int, but null given");
				return;
			}
			
			MinCount = -1;
			MinCountGetter = intGetter;
		}
	}
}
