using System;
using System.Diagnostics.CodeAnalysis;


namespace Valhalla.OdinAttributes
{
	[AttributeUsage(AttributeTargets.Field)]
	[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
	public class ParentsOnlyAttribute : Attribute
	{
		
	}
}
