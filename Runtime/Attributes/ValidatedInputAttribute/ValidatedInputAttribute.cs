using System;


namespace Valhalla.OdinAttributes
{
	public abstract class ValidatedInputAttribute : Attribute
	{
		public string InputErrorMessage { get; private set; } = null;
		
		public bool IsInputInvalid
			=> !string.IsNullOrEmpty(InputErrorMessage);


		protected void SetInputError(string errorMessage)
		{
			InputErrorMessage = errorMessage;
		}
	}
}
