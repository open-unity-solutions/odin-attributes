using Sirenix.OdinInspector.Editor.Validation;


namespace Valhalla.OdinAttributes.Editor
{
	public abstract class ValidatedInputValidator<TAttribute, TValue> : AttributeValidator<TAttribute, TValue>
		where TAttribute : ValidatedInputAttribute
	{
		protected override void Validate(ValidationResult result)
		{
			if (Attribute.IsInputInvalid)
				result.AddError(Attribute.InputErrorMessage);
		}
	}
}
