using System;
using System.Linq;
using Valhalla.OdinAttributes.Editor;
using Sirenix.OdinInspector.Editor.Validation;
using UnityEditor;
using UnityEngine;


[assembly: RegisterValidator(typeof(ParentsOnlyValidator<>))]
namespace Valhalla.OdinAttributes.Editor
{
	public class ParentsOnlyValidator<TValue> : AttributeValidator<ParentsOnlyAttribute, TValue>
		where TValue : Component
	{
		protected override void Validate(ValidationResult result)
		{
			if (Value == null)
				return;
			
			var ownedByComponent = Property.Parent.ValueEntry.WeakSmartValue;
			
			if (ownedByComponent is not Component ownerComponent)
			{
				result.AddError("Owner is not a Component");
				return;
			}

			var owner = ownerComponent.gameObject;
			bool isValueParentOfOwner = SearchInChildren(Value.transform, owner);

			if (!isValueParentOfOwner)
				result
					.AddError($"Value must be parent of an object in hierarchy")
					.WithFix("Try add component from parents", () => FindInParent(ownerComponent.transform, Property.ValueEntry.TypeOfValue));
		}


		private static bool SearchInChildren(Transform parent, GameObject lookingFor)
		{
			foreach (Transform child in parent)
			{
				if (child.gameObject == lookingFor)
					return true;

				if (SearchInChildren(child, lookingFor))
					return true;
			}

			return false;
		}
		
		
		private void FindInParent(Transform owner, Type type)
		{
			Value = owner.GetComponentInParent(type) as TValue;
			Property.Tree.ApplyChanges();
			AssetDatabase.SaveAssets();
		}
	}
}
