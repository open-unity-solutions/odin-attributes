using System.Linq;
using Valhalla.OdinAttributes.Editor;
using Sirenix.OdinInspector.Editor.Validation;
using UnityEditor;
using UnityEngine;


[assembly: RegisterValidator(typeof(SelfOnlyValidator<>))]
namespace Valhalla.OdinAttributes.Editor
{
	public class SelfOnlyValidator<TValue> : AttributeValidator<SelfOnlyAttribute, TValue>
		where TValue : Component
	{
		protected override void Validate(ValidationResult result)
		{
			var ownedBy = Property.SerializationRoot.ValueEntry.WeakSmartValue;
			
			
			if (ownedBy is not Component ownerComponent)
			{
				if (ownedBy == null)
				{
					result.AddError("Owner is null, can't .GetComponent");
					return;
				}
				
				result.AddError($"Owner's type {ownedBy.GetType()} is not a Component");
				return;
			}
			
			
			var ownerGameObject = ownerComponent.gameObject;
			var propertyTypedComponents = ownerGameObject.GetComponents<TValue>();
			if (propertyTypedComponents.Length == 0)
			{
				result
					.AddError($"No component of type {ValueEntry.ParentType} found on {ownerGameObject.name}")
					.WithFix("Add component to Self", () => SetPropertyValue(ownerGameObject.AddComponent<TValue>()));
				return;
			}
			
			if (Value is null)
			{
				result
					.AddWarning("Property is null")
					.WithFix("Get Self", () => SetPropertyValue(propertyTypedComponents[0]));
				return;
			}

			if (!propertyTypedComponents.Contains(Value))
				result
					.AddError($"Value of type <{ValueEntry.ParentType}> is not component of {ownerGameObject.name}")
					.WithFix("Replace with Self", () => SetPropertyValue(propertyTypedComponents[0]));
		}
		
		
		private void SetPropertyValue(TValue value)
		{
			Value = value;
			Property.Tree.ApplyChanges();
			AssetDatabase.SaveAssets();
		}
	}
}
