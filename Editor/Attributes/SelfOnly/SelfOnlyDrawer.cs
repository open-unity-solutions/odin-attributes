using System.Diagnostics.CodeAnalysis;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using UnityEditor;
using UnityEngine;
using Valhalla.DisposableLayouts.Editor;


namespace Valhalla.OdinAttributes.Editor
{
	[SuppressMessage("ReSharper", "UnusedType.Global")]
	public class SelfOnlyDrawer<TValue> : OdinAttributeDrawer<SelfOnlyAttribute, TValue>
		where TValue : Component
	{
		private const string ButtonGetText = "Get";
		private const string ButtonAddText = "Add";
		private const float ButtonMinWidth = 30f;
		

		
		
		protected override void DrawPropertyLayout(GUIContent label)
		{
			using (EditorGUILayoutHorizontal.Create())
			{
				using (EditorGUILayoutVertical.Create())
					CallNextDrawer(label);
				
				
				var ownedBy = Property.SerializationRoot.ValueEntry.WeakSmartValue;
				if (ownedBy is not Component ownerComponent)
					return;

				var ownerGameObject = ownerComponent.gameObject;
				var propertyTypedComponents = ownerGameObject.GetComponents<TValue>();

				bool wasClickPerformed = false;
				if (propertyTypedComponents.Length == 0)
				{
					if (GUILayout.Button(ButtonAddText, EditorStyles.miniButton, GUILayoutOptions.ExpandWidth(false).MinWidth(ButtonMinWidth)))
					{
						Property.RecordForUndo($"Click {ButtonAddText}");
						ValueEntry.SmartValue = (TValue) ownerGameObject.AddComponent(ValueEntry.TypeOfValue);
						wasClickPerformed = true;
					}
				}
				else
				{
					if (GUILayout.Button(ButtonGetText, EditorStyles.miniButton, GUILayoutOptions.ExpandWidth(false).MinWidth(ButtonMinWidth)))
					{
						Property.RecordForUndo($"Click {ButtonGetText}");
						ValueEntry.SmartValue = propertyTypedComponents[0];
						wasClickPerformed = true;
					}
				}
				
				if (wasClickPerformed)
				{
					Property.Tree.ApplyChanges();
					AssetDatabase.SaveAssets();
				}
			}

		}
	}
}
