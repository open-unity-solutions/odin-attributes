
using System.Collections;
using Valhalla.OdinAttributes.Editor;
using Sirenix.OdinInspector.Editor.Validation;
using Sirenix.OdinInspector.Editor.ValueResolvers;


[assembly: RegisterValidator(typeof(ItemCountValidator<>))]
namespace Valhalla.OdinAttributes.Editor
{
	public class ItemCountValidator<TValue> : ValidatedInputValidator<ItemCountAttribute, TValue>
		where TValue : ICollection
	{

		private ValueResolver<int> _validCountGetter;


		protected override void Initialize()
		{
			_validCountGetter = ValueResolver.Get(Property.ParentValueProperty, Attribute.ValidCountGetter, Attribute.ValidCount);
		}
		

		protected override void Validate(ValidationResult result)
		{
			base.Validate(result);

			if (_validCountGetter.HasError)
			{
				result.AddError(_validCountGetter.ErrorMessage);
				return;
			}
			
			if (Attribute.IsInputInvalid)
				return;
			
			if (Value == null)
				return;


			int validCount = _validCountGetter.GetValue();
			
			if (Value.Count != validCount)
				result.AddError($"Required item count: {validCount}, but {Value.Count} given");
		}

	}
}
