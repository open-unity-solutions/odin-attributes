
using System.Collections;
using Valhalla.OdinAttributes.Editor;
using Sirenix.OdinInspector.Editor.Validation;
using Sirenix.OdinInspector.Editor.ValueResolvers;


[assembly: RegisterValidator(typeof(MaxItemCountValidator<>))]
namespace Valhalla.OdinAttributes.Editor
{
	public class MaxItemCountValidator<TValue> : ValidatedInputValidator<MaxItemCountAttribute, TValue>
		where TValue : ICollection
	{
		private ValueResolver<int> _maxCountGetter;
		
		
		protected override void Initialize()
		{
			_maxCountGetter = ValueResolver.Get(Property.ParentValueProperty, Attribute.MaxCountGetter, Attribute.MaxCount);
		}
		
		
		protected override void Validate(ValidationResult result)
		{
			base.Validate(result);
			
			if (_maxCountGetter.HasError)
			{
				result.AddError(_maxCountGetter.ErrorMessage);
				return;
			}
			
			if (Attribute.IsInputInvalid)
				return;
			
			if (Value == null)
				return;
			
			
			int maxCount = _maxCountGetter.GetValue();

			if (Value.Count > maxCount)
				result.AddError($"Max item count: {maxCount}, but {Value.Count} given");
		}
	}
}
