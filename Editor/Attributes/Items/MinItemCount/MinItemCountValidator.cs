using System.Collections;
using Valhalla.OdinAttributes.Editor;
using Sirenix.OdinInspector.Editor.Validation;
using Sirenix.OdinInspector.Editor.ValueResolvers;


[assembly: RegisterValidator(typeof(MinItemCountValidator<>))]
namespace Valhalla.OdinAttributes.Editor
{
	public class MinItemCountValidator<TValue> : ValidatedInputValidator<MinItemCountAttribute, TValue>
		where TValue : ICollection
	{
		private ValueResolver<int> _minCountGetter;
		
		
		protected override void Initialize()
		{
			_minCountGetter = ValueResolver.Get(Property.ParentValueProperty, Attribute.MinCountGetter, Attribute.MinCount);
		}
		
		
		protected override void Validate(ValidationResult result)
		{
			base.Validate(result);
			
			if (_minCountGetter.HasError)
			{
				result.AddError(_minCountGetter.ErrorMessage);
				return;
			}
			
			if (Attribute.IsInputInvalid)
				return;
			
			if (Value == null)
				return;
			
			
			int minCount = _minCountGetter.GetValue();

			if (Value.Count < minCount)
				result.AddError($"Min item count: {minCount}, but {Value.Count} given");
		}
	}
}
