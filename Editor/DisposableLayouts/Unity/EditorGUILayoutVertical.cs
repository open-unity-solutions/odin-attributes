using UnityEditor;
using UnityEngine;


namespace Valhalla.DisposableLayouts.Editor
{
	public class EditorGUILayoutVertical : IDisposableLayout<GUILayoutOption[]>
	{
		public static EditorGUILayoutVertical Create(params GUILayoutOption[] options)
		{
			var layout = new EditorGUILayoutVertical();
			layout.Begin(options);
			return layout;
		}

		
		public void Begin(GUILayoutOption[] options)
			=> EditorGUILayout.BeginVertical(options);

		public void Dispose()
			=> EditorGUILayout.EndVertical();
	}
}
