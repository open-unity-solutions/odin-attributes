using UnityEditor;
using UnityEngine;


namespace Valhalla.DisposableLayouts.Editor
{
	public class EditorGUILayoutHorizontal : IDisposableLayout<GUILayoutOption[]>
	{
		public static EditorGUILayoutHorizontal Create(params GUILayoutOption[] options)
		{
			var layout = new EditorGUILayoutHorizontal();
			layout.Begin(options);
			return layout;
		}

		
		public void Begin(GUILayoutOption[] options)
			=> EditorGUILayout.BeginHorizontal(options);

		public void Dispose()
			=> EditorGUILayout.EndHorizontal();
	}
}
