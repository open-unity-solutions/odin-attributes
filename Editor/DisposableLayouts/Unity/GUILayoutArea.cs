using UnityEngine;


namespace Valhalla.DisposableLayouts.Editor
{
	public class GUILayoutArea : 
		IDisposableLayout<Rect>,
		IDisposableLayout<Rect, GUIContent>,
		IDisposableLayout<Rect, GUIContent, GUIStyle>,
		IDisposableLayout<Rect, GUIStyle>,
		IDisposableLayout<Rect, Texture>,
		IDisposableLayout<Rect, Texture, GUIStyle>,
		IDisposableLayout<Rect, string>
	{
		
		public static GUILayoutArea Create(Rect arg1)
		{
			var area = new GUILayoutArea();
			area.Begin(arg1);
			return area;
		}


		public static GUILayoutArea Create(Rect arg1, GUIContent arg2)
		{
			var area = new GUILayoutArea();
			area.Begin(arg1, arg2);
			return area;
		}
		

		public static GUILayoutArea Create(Rect arg1, GUIContent arg2, GUIStyle arg3)
		{
			var area = new GUILayoutArea();
			area.Begin(arg1, arg2, arg3);
			return area;
		}


		public static GUILayoutArea Create(Rect arg1, GUIStyle arg2)
		{
			var area = new GUILayoutArea();
			area.Begin(arg1, arg2);
			return area;
		}


		public static GUILayoutArea Create(Rect arg1, Texture arg2)
		{
			var area = new GUILayoutArea();
			area.Begin(arg1, arg2);
			return area;
		}


		public static GUILayoutArea Create(Rect arg1, Texture arg2, GUIStyle arg3)
		{
			var area = new GUILayoutArea();
			area.Begin(arg1, arg2, arg3);
			return area;
		}


		public static GUILayoutArea Create(Rect arg1, string arg2)
		{
			var area = new GUILayoutArea();
			area.Begin(arg1, arg2);
			return area;
		}

		
		
		public void Begin(Rect arg1)
			=> GUILayout.BeginArea(arg1);


		public void Begin(Rect arg1, GUIContent arg2)
			=> GUILayout.BeginArea(arg1, arg2);


		public void Begin(Rect arg1, GUIContent arg2, GUIStyle arg3)
			=> GUILayout.BeginArea(arg1, arg2, arg3);


		public void Begin(Rect arg1, GUIStyle arg2)
			=> GUILayout.BeginArea(arg1, arg2);


		public void Begin(Rect arg1, Texture arg2)
			=> GUILayout.BeginArea(arg1, arg2);


		public void Begin(Rect arg1, Texture arg2, GUIStyle arg3)
			=> GUILayout.BeginArea(arg1, arg2, arg3);


		public void Begin(Rect arg1, string arg2)
			=> GUILayout.BeginArea(arg1, arg2);


		public void Dispose()
			=> GUILayout.EndArea();
	}
}
