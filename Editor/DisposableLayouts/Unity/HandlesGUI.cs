using UnityEditor;
using UnityEngine;


namespace Valhalla.DisposableLayouts.Editor
{
	// No `Handles.BeginGUI(Rect rect)` overload, because it's obsolete
	public class HandlesGUI : IDisposableLayout
	{
		public static HandlesGUI Create()
		{
			var gui = new HandlesGUI();
			gui.Begin();
			return gui;
		}


		public void Begin()
			=> Handles.BeginGUI();


		public void Dispose()
			=> Handles.EndGUI();
	}
}
