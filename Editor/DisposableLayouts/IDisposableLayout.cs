using System;


namespace Valhalla.DisposableLayouts.Editor
{
	public interface IDisposableLayout : IDisposable
	{
		void Begin();
	}

	
	public interface IDisposableLayout<in TArg1> : IDisposable
	{
		void Begin(TArg1 arg1);
	}
	

	public interface IDisposableLayout<in TArg1, in TArg2> : IDisposable
	{
		void Begin(TArg1 arg1, TArg2 arg2);
	}

	
	public interface IDisposableLayout<in TArg1, in TArg2, in TArg3> : IDisposable
	{
		void Begin(TArg1 arg1, TArg2 arg2, TArg3 arg3);
	}
	
	
	public interface IDisposableLayout<in TArg1, in TArg2, in TArg3, in TArg4> : IDisposable
	{
		void Begin(TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4);
	}
}
