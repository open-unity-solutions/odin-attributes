## [1.5.3] – 2023-07-25

### Fixed
- `[SelfOnly]` drawer now saves asset only after actual click, not every frame
- CI/CD refs updated to match current configuration


## [1.5.2] - 2023-06-16

### Added
- String expression input added to `[ItemCount]`, `[MinItemCount]` and `[MaxItemCount]` attributes

### Changed
- Licence year updated


## [1.5.1]

### Added
- `GUILayout.BeginArea()`/`GUILayout.EndArea()` are now supported

### Changed
- Internal API changes


## [1.5.0]

### Added
- `GUILayoutOption`s are now supported on Horizontal and Vertical layouts
- `Handles.BeginGUI()`/`Handles.EndGUI()` are now supported

### Changed
- Creation of layouts via factory method `.Create()`, not a constructor. [!] Breaking change.

### Fixed
- Namespace of layouts changed to `Valhalla`


## [1.4.1]

### Fixed
- `[SelfOnly]` now works with trees, not only the root object


## [1.4.0]

### Added
- Validation of item count by Enum type

### Changed
- Rebranded to Valhalla


## [1.3.0]

### Added
- `[ItemCount]` attribute
- `[MaxItemsCount]` attribute
- `[MinItemsCount]` attribute

### Fixed
- Resharper warning message about zero instances removed

### Changed
- Changelog format updated


## [1.2.1]

### Fixed
- Removed some unused code
- Fixed behaviour of `[ParentsOnly]` when property is null


## [1.2.0]

### Added
- `[ParentsOnly]` attribute with validation
- `[SelfOnly]' now adds component if needed

### Changed
- Runtime now has no separate folders for each attribute
- `SelfOnlyAttributeDrawer` updated to match validator

### Fixed
- Spelling of "self" in directory name


## [1.1.0]

### Added
- `SelfOnlyValidator` now supports Fixes from Odin Validator

### Changed
- `SelfOnlyAttributeDrawer` updated to Odin 3.1.x


## [1.0.0]

### Added
- `[SelfOnly]` attribute with set button and validation 
- Disposable wrappers (`EditorGUILayoutHorizontal` and `EditorGUILayoutVertical`) around `EditorGUILayout` for block-style syntax
